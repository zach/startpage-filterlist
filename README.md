### I am not affiliated with Startpage or Swisscows. I just like their search engine and want to support them. I hope you enjoy this filterlist! :)

# Filterlist
Ublock Origin filter to support [Startpage](https://www.startpage.com) and [Swisscows](https://www.swisscows.com) by whitelisting their ads. 

# Why Whitelist Their Ads?
Both Startpage and Swisscows use privacy respecting contextual based advertisements. 

Startpage business model ([blog post](https://www.startpage.com/privacy-please/startpage-articles/whats-startpages-revenue-model))

Swisscows business model ([Advertising at Swisscows](https://swisscows.com/en/privacy))

If you do not want to whitelist ads, just don't use this filterlist. Ublock Origin (with default settings) should block any and all ads.